# Budget

Documentation du schéma de données des informations budgétaires des collectivités et établissements publics locaux

- nom: scdl-budget
- page d'accueil: https://gitlab.com/opendatafrance/scdl/budget
- URL du schéma: https://gitlab.com/opendatafrance/scdl/budget/raw/v0.8.3/schema.json
- version: 0.8.3
- date de création: 07/11/2019
- date de dernière modification: 28/09/2023
- concerne le pays: FR
- valeurs manquantes représentées par: `[""]`
- contributeurs:
  - OpenDataFrance (auteur)
  - Pascal Romain (Rhizome data)
  - Amélie Rondot (multi)
- ressources:
  - Exemple de fichier budgétaire valide ([lien](https://gitlab.com/opendatafrance/scdl/budget/raw/v0.8.3/examples/budget_valide.csv)

## Contexte

Les fichiers <DocumentBudgetaire> sont produits par les collectivités territoriales et autres établissements publics dans le cadre du projet Actes Budgétaires qui se repose sur la plateforme @ctes (Présentation technique).
Afin d'en simplifier la publication, nous proposons une version simplifiée du format XML proposé par le ministère de l’Économie et du budget en lien avec la Direction Générale des Collectivités Locales (DGCL) En complément, un outil de transformation des fichiers XML "TOTEM" en fichiers csv pourra être proposé afin de faciliter la génération de ces fichiers de publication simplifiés.
Ce schéma reprend les données de la section LigneBudget du schéma Totem. Il permet de rattacher un montant de recette ou de dépense à une nature (quoi?) et une fonction (pourquoi ?) comptable.

## Modèle de données

Ce modèle de données fait partie et respecte les exigences du Socle Commun des Données Locales. Il repose sur les 6 champs obligatoires et 9 champs facultatifs suivants correspondant aux colonnes du fichier tabulaire.

### `BGT_NATDEC`

- Titre : étape budgétaire
- Description : Ce champ permet de déterminer l'étape budgétaire concernée. Les valeurs possibles sont : \"compte administratif\", \"budget prévisionnel\", \"budget supplémentaire\", \"décision modificative\". Les valeurs possibles sont : "compte administratif (09), "budget primitif (01), "budget supplémentaire (03), "décision modificative (02)
- Type : texte
- Exemple : `compte administratif`
- Valeur : obligatoire
- Valeurs acceptées : "budget primitif", "budget supplémentaire","décision modificative","compte administratif"

### `BGT_ANNEE`

- Titre : année de l'exercice
- Description : Ce champ permet de renseigner l'année de l'exercice budgétaire concerné. 
- Type : year
- Exemple : `2018`
- Valeur : obligatoire

### `BGT_SIRET`

- Titre : code SIRET
- Description : Ce champ permet de renseigner le code SIRET de la collectivité ou de l'établissement public concerné (http://xml.insee.fr/schema/siret.html#SIRET_stype).
- Type : string
- Exemple : `22192720500197`
- Valeur : obligatoire
- Motif : `\\d{14}$`

### `BGT_NOM`

- Titre : nom de la collectivité
- Description : En complément du code SIRET, ce champ permet de faciliter l'identification de l'organisme public concerné.
- Type : texte
- Exemple : `Département de la Corrèze`
- Valeur : obligatoire

### `BGT_CONTNAT`

- Titre : numéro du compte budgétaire
- Description : Ce champ correspond au numéro de l'article ou compte budgétaire. Le référentiel des comptes budgétaires est fourni par le ministère de l'Economie. A partir de la nomemclature comptable de chaque type de collectivité ou établissement local, le plan de compte contient la liste des codes associés à chaque maquette budgetaire.
- Type : numérique
- Exemple : `04`
- Valeur : obligatoire
- Longueur minimale : 1
- Longueur maximale : 9

### `BGT_CONTNAT_LABEL`

- Titre : Libellé du compte budgétaire
- Description : Ce champ correspond à la colonne LIBELLE de l'article ou compte budgétaire dans le tableau \"Liste des comptes et utilisations\" du plan de compte.
- Type : string
- Exemple : `Agencements et aménagements de terrains`
- Valeur : obligatoire

### `BGT_NATURE`

- Titre : Nature de la dépense ou de la recette
- Description : Ce champ correspond à la nature de la dépense ou de la recette, c'est-à-dire au \"quoi\". Classer les dépenses par nature signifie que les recettes et les dépenses sont regroupées selon leur identité financière. Le numéro est un numéro de chapitre. Exemples de chapitre : Charges à caractère général, Charges financières…(voir plan de compte)
- Type : string
- Exemple : `21343`
- Valeur : obligatoire
- Longueur minimale : 1
- Longueur maximale : 9

### `BGT_NATURE_LABEL`

- Titre : Libellé de la nature de la dépense ou de la recette
- Description : Ce champ correspond à la colonne LIBELLE de la liste des chapitres du plan de compte par nature.
- Type : string
- Exemple : `Matériel scolaire`
- Valeur : obligatoire


### `BGT_FONCTION`

- Titre : Fonction de la dépense ou de la recette
- Description : Ce champ correspond à la fonction de la dépense ou de la recette, c'est-à-dire le \"pourquoi\". Classer les dépenses et les recettes par fonction consiste à les regrouper à partir de leurs destinations ou encore de leurs finalités. Le numéro est un numéro de chapitre. Exemples de fonction : Action sociale, Aménagement et environnement...(voir plan de compte).
- Type : string
- Exemple : `04`
- Valeur : obligatoire
- Longueur minimale : 1
- Longueur maximale : 9

### `BGT_FONCTION_LABEL`

- Titre : Libellé de la fonction de la dépense ou de la recette
- Description : Ce champ correspond à la colonne LIBELLE de la liste des chapitres du plan de compte par fonction.
- Type : string
- Exemple : `éducation`
- Valeur : obligatoire

### `BGT_OPERATION`

- Titre : Code ou libellé de l'opération budgétaire votée
- Description : Ce champ correspond à une opération budgétaire (un projet particulier) sur lequel on souhaite attirer l'attention notamment lors du vote. Dans le cas d'une opération "Pour Vote", ce compte opération doit contenir uniquement des chiffres ; dans le cas d'une opération "Pour information", c'est libre.
- Type : string
- Exemple : `Aménagement du parc Paul Mistral`
- Valeur : optionnelle

### `BGT_SECTION`

- Titre : section budgétaire
- Description : Ce champ correspond au type de recette ou de dépense. A partir du Plan de Compte, dans le tableau \"Liste des comptes et utilisations\", utiliser le ContNat comme code et voir le chapitre correspondant dans la colonne RR ou DR pour une recette ou une dépense respectivement. Pour savoir s'il s'agit de Fonctionnement ou d'Investissement, regarder dans le tableau \"Liste des chapitres\" : utiliser le code de chapitre et regarder la colonne SECTION.
- Type : string
- Exemple : `investissement`
- Valeur : obligatoire
- Valeurs autorisées : investissement ou fonctionnement

### `BGT_OPBUDG`

- Titre : type d'opération budgétaire
- Description : Ce champ permet de distinguer les opérations d'ordre budgétaire des opérations réelles. Opérations d'ordre : qui ne donnent pas lieu à encaissement ou décaissement ; à la différence des opérations réelles.
- Type : string
- Exemple : `réel`
- Valeur : obligatoire
- Valeur possibles : réel ou ordre 

### `BGT_CODRD`

- Titre : Code recette / dépense
- Description : Ce champ permet de déterminer le sens (recette 0 - dépense 1) du crédit concerné.
- Type : string
- Exemple : `recette`
- Valeur : obligatoire. 
- Valeurs possibles : recette ou dépense

### `BGT_MTREAL`

- Titre : Montant signé des réalisations budgétaires
- Description : Ce champ correspond au montant réalisé (en dépense ou en recette). Ce champ est à renseigner uniquement pour un compte administratif.
- Type : numérique
- Exemple : `45 678,80`
- Valeur: optionnelle

### `BGT_MTBUDGPREC`

- Titre : Montant du budget précédent
- Description : Ce champ correspond au montant prévu lors de l'exercice précédent. Ce champ permet de voir les évolutions des montants entre le budget prévisionnel (BP), les décisions modificatives et le compte administratif qui consacre la réalité des montants effectivement engagés (payés ou reçus).
- Type : numérique
- Exemple : `55 678,80`
- Valeur: optionnelle

### `BGT_MTRARPREC`

- Titre : Montant restant à réaliser N-1.
- Description : Ce champ est correspond aux montant des reprises des résultats des exercices précédents. Ce champ est rempli uniquement au BP/BS/DM et au CA pour présenter les restes à réaliser des exercice précédents.
- Type : numérique
- Exemple : `45 678,80`
- Valeur: optionnelle

### `BGT_MTPROPNOUV`

- Titre : Montant "Propositions nouvelles"
- Description : Ce champ correspond au montant des recettes ou dépenses nouvelles non prévues dans les étapes budgétaires précédentes.
- Type : numérique
- Exemple : `45 678,80`
- Valeur: optionnelle

### `BGT_MTPREV`

- Titre : montant budget voté 
- Description : Ce champ correspond au montant prévu lors des exercices BP/BS/DM pécédents. En présence d'une valeur dans ce champ, celui-ci doit obligatoirement être valorisée soit du montant prévu soit d'un montant égal à zéro.
- Type : numérique
- Exemple : `45 678,80`
- Valeur: optionnelle

### `BGT_CREDOUV`

- Titre : crédits ouverts à l'article
- Description : Ce champ correspond au montant des crédits de paiement disponibles pour effectuer des dépenses sur cet article.
- Type : numérique
- Exemple : `45 678,80`
- Valeur: optionnelle

### `BGT_MTRAR3112`

- Titre : Montant reste à réaliser de l’exercice
- Description : ce champ est utilisé pour le compte administratif. Il peut également être utilisé au BP/BS/DM pour présenter les informations du compte administratif des années précédentes
- Type : numérique
- Exemple : `45 678,80`
 -Valeur: optionnelle

### `BGT_ARTSPE`

- Titre : Article spécialisé
- Description : Ce champ permet d'isoler des articles au moment du vote du budget. 
- Type : string
- Exemple : `article non spécialisé`
- Valeur : optionnelle 
- Valeurs autorisées article non spécialisé ou article spécialisé
